package com.example.brtest1

import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Build
import android.os.IBinder
import android.telephony.TelephonyManager
import android.widget.Toast
import androidx.core.app.NotificationCompat

class BroadcastService : Service() {
    override fun onBind(arg0: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        val intentFilter = IntentFilter()
        intentFilter.priority = 2147483647
        intentFilter.addAction("android.intent.action.PHONE_STATE")
        br_StateChangeReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action == TelephonyManager.ACTION_PHONE_STATE_CHANGED) {
                    // do the stuff here
                    Toast.makeText(context, "BR Triggered", Toast.LENGTH_LONG).show()
                    println("Br receiveddd...")
                    val intent1 = Intent()
                    intent1.setClass(context, MainActivity::class.java)
                    intent1.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    context.startActivity(intent1)
                }
            }
        }
        registerReceiver(br_StateChangeReceiver, intentFilter)
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        if (intent.action == "START") {
            val callServiceNotificationIntent = Intent(this, MainActivity::class.java)
            callServiceNotificationIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            val pendingIntent = PendingIntent
                    .getActivity(this, 1,
                            callServiceNotificationIntent, PendingIntent.FLAG_ONE_SHOT)
            val NOTIFICATION_CHANNEL_ID = "com.example.simpleapp"
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channelName = "My Background Service"
                val chan = NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE)
                chan.lightColor = Color.BLUE
                chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
                val manager = (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
                manager.createNotificationChannel(chan)
            }
            val notification = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                    .setContentTitle("BR_Activity")
                    .setContentText("hello")
                    .setContentIntent(pendingIntent)
                    .setOngoing(true)
                    .build()
            startForeground(1, notification)
        }
        return START_STICKY
    }

    companion object {
        private var br_StateChangeReceiver: BroadcastReceiver? = null
    }
}